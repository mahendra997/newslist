import { Component, OnInit, Input } from '@angular/core';
import *as moment from 'moment';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input()
  item;

  time = '';

  constructor() { }

  ngOnInit() {
    this.time = moment(this.item.publishedAt, 'YYY-MM-DDTHH:mm:ssZ').format('MMMM Do YY').toString();
  }

  openUrl() {
    window.open(this.item.url, '_blank');

    // window.location.href = "url";
  }

}

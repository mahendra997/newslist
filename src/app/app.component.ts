import { Component, OnInit, OnDestroy } from '@angular/core';
import { NetworkService } from './services/network/network.service';
import { ActivatedRoute, Router } from '@angular/router';
import { timer, Observable, interval, Subscription } from 'rxjs';
import { switchMap, takeUntil, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public searchData = {
    query: '',
  };

  title = 'News Update';

  newsArray = [];
  sum = 100;
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  page = 1;
  queryValue = '';
  counter = 1;
  info = 'Waiting for your search!';
  showInfo = true;

  timerSource = timer(0, 30000);
  timerSubscription = this.timerSource.pipe(switchMap(() => interval(1000)));

  constructor(
    private networkService: NetworkService,
    private route: ActivatedRoute,
    private router: Router) {

    this.route.queryParams.subscribe(params => {
      if (this.queryValue !== params['q']) {
        this.page = 1;
      }
      this.queryValue = params['q'];
      this.showInfo = true;
      this.info = 'Loading...';
      networkService.getNews(this.queryValue, this.page).subscribe(
        res => {
          this.showInfo = false;
          this.page++;
          this.newsArray = res.articles;
          this.showInfo = false;
          this.info = '';
        }, err => {
          console.log(err);
          this.showInfo = true;
          this.info = 'Failed to Load!';
        }
      );
    });
  }

  onScrollDown() {
    this.showInfo = true;
    this.info = 'Loading...';
    this.networkService.getNews(this.queryValue, this.page).subscribe(
      res => {
        this.showInfo = false;
        this.info = '';
        this.page++;
        res.articles.forEach(element => {
          this.newsArray.push(element);
        });
      }, err => {
        this.showInfo = true;
        this.info = 'Failed to Load!';
        console.log(err);
      }
    );
  }

  onSearchSubmit() {
    if (this.searchData.query.length < 1) {
      this.info = 'Bad search :(';
      this.showInfo = true;
      this.newsArray = [];
    } else {
      this.router.navigate(['/'], { queryParams: { q: this.searchData.query } });
    }
  }

  ngOnInit() {
    this.timerSubscription.subscribe(val => {
      this.counter = 30 - val;
      if (val === 0) {
        this.showInfo = true;
        this.info = 'Loading...';
        this.networkService.getNews(this.queryValue, this.page).subscribe(
          res => {
            this.showInfo = false;
            this.info = '';
            this.page = 1;
            this.newsArray = res.articles;
          }, err => {
            this.showInfo = true;
            this.info = 'Failed to Load!';
            console.log(err);
          }
        );
      }
    });
  }
}

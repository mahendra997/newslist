import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  apiKey = '363d26dd3d664d199ca63adc371e22aa';

  constructor(private http: HttpClient) { }

  public getNews(query, page) {
    return this.http.get<any>('https://newsapi.org/v2/everything?q=' + query +
      '&apiKey=' + this.apiKey +
      '&pageSize=10&page=' + page);
  }
}
